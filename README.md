# tuk_tuk

![photo of assembled tuk_tuk keyboard](assets/tuk_tuk_song_med.jpg)

### a BIG thanks to

- Noah Kiser
  - [TKL Keyboard PCB Design, Part 1](https://www.youtube.com/watch?v=6Z49bynRqj8)
  - [TKL Keyboard PCB Design, Part 2](https://www.youtube.com/watch?v=YsHCpA9_U6s)
  - [STM32 keyboard PCB Design](https://www.youtube.com/watch?v=kGKN8SGkUD0)
- [QMK Firmware](https://github.com/qmk/qmk_firmware)
- [Vial Firmware](https://get.vial.today/docs/)
- [JLCPCB’s fabrication and assembly services](https://jlcpcb.com/)

### A katana-stagger 42-key PCB designed to fit into a Minivan case:
- symmetric split-ergo style layout (6 × 3 + 3)
- katana stagger
  - personally find this works really nicely and keeps the feel of an ortho or columnar board
- fit into Minivan case (12.75u × 4u)
- attempt to use as many existing mounting holes as possible (6/7)
  - main issue was maintaining symmetric layout with two mounting points in bottom row
  - personally find the resulting odd gap to be uncomfortable
- learning to use JLC’s PCB assembly instead of a Pro Micro equivalent

## [tuk_tuk nueng](nueng) iter 1

## [tuk_tuk song](song) iter 2

## [tuk_tuk sam](sam) iter 3
