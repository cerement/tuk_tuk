# tuk_tuk song

![photo of assembled tuk_tuk song keyboard](../assets/tuk_tuk_song_med.jpg)

- didn’t like the 1u gaps on the bottom row of the tuk_tuk nueng
- when the gaps were removed, I had to shift whole bottom row 0.125u to the left to fit in between the two bottom row mounting points
- tried to embrace the assymetry by changing the 2.25u–2.25u bottom row to 1.75u–2.75u sizes
  - not entirely a success, personally find 2.25u–2.25u feels better in practice
  - will have to tolerate the 0.125u offset

## files
- [KLE layout](tuk_tuk_song_layout.json)
- PCB design
  - [KiCad schematic](kicad/tuk_tuk_song.kicad_sch)
  - [KiCad PCB](kicad/tuk_tuk_song.kicad_pcb)
  - [KiCad plate](kicad/tuk_tuk_song_plate.kicad_pcb)
- PCB fabrication
  - [PCB Gerbers](pcb_fabrication/tuk_tuk_song_gerbers.zip)
  - [PCB bill-of-materials](pcb_fabrication/tuk_tuk_song_bom.csv)
  - [PCB pick-and-place](pcb_fabrication/tuk_tuk_song-all-pos.csv)
  - [plate Gerbers](pcb_fabrication/tuk_tuk_song_plate_gerbers.zip)
- [Vial firmware](tuk_tuk_song_vial.uf2)
