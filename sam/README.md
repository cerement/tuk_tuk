# tuk_tuk sam

![drawing of tuk_tuk sam layout](../assets/tuk_tuk_sam_chopper.png)

### notes
- modified for 13u layout
  - to fit [HIBI Design’s Chopper](https://hibi.mx/products/chopper)
  - extra 0.25u spacing in center compared to previous
- (waiting on shipment to confirm layout and USB placement)
