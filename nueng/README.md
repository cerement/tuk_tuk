# tuk_tuk nueng

![photo of assembled tuk_tuk keyboard](../assets/tuk_tuk-02.jpg)

## files
- [layout](tuk_tuk-full-layout.json)
- PCB design
  - [KiCad schematic](tuk_tuk.kicad_sch)
  - [KiCad PCB](tuk_tuk.kicad_pcb)
- PCB fabrication
  - [Gerbers](pcb_fabrication/tuk_tuk_gerbers.zip)
  - [bill-of-materials](pcb_fabrication/tuk_tuk_bom.csv)
  - [pick-and-place](pcb_fabrication/tuk_tuk-bottom-pos.csv)
- [Vial firmware](tuk_tuk_vial.uf2)


![photo of PCB for the tuk_tuk keyboard](../assets/tuk_tuk-01.jpg)
